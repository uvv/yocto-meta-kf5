# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/portingAids/kdesignerplugin-5.87.0.tar.xz"
SRC_URI[sha256sum] = "a92af15be904629fc98c21fa31f132d3697f62df33af5e37f3ac70f3369e2c8b"

