# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/portingAids/kdelibs4support-5.87.0.tar.xz"
SRC_URI[sha256sum] = "4e0ad7ecfafb1663b1eaa0e37068da0bb52d958687b5a1ac6f309fd49c397c9e"

