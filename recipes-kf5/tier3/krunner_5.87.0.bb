# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/krunner-5.87.0.tar.xz"
SRC_URI[sha256sum] = "3aac094456fc3da851f58842604a28bebafaf4ef05359aa102e3f6e4a272f879"

