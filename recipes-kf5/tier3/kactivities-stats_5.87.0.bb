# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kactivities-stats-5.87.0.tar.xz"
SRC_URI[sha256sum] = "0604179842ab3a44018335769bf6b8981c1d1d40a176ad6db694f1c7ddfaf2d8"

