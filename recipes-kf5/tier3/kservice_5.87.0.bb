# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kservice-5.87.0.tar.xz"
SRC_URI[sha256sum] = "b657640c4d93bb18af16b3053f7e7532e89021ba079de079f21cffc521666e28"

