# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kemoticons-5.87.0.tar.xz"
SRC_URI[sha256sum] = "befc25576de12ebf7a167f754bf962a504f7049a22c60c40aab6a70e34375045"

