# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kinit-5.87.0.tar.xz"
SRC_URI[sha256sum] = "ba07d4bd871d0ed0e758fb8d74f68e08e1a68aae71312c105e4fe908b7d76510"

