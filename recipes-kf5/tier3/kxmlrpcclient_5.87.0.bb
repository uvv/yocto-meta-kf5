# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/portingAids/kxmlrpcclient-5.87.0.tar.xz"
SRC_URI[sha256sum] = "a7298f56d5b3997b3765116ec1e3f09e315b21ca004c0d03e0c3894289fe850e"

