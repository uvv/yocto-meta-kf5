# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kdeclarative-5.87.0.tar.xz"
SRC_URI[sha256sum] = "6792f8ee5e255159d302ba8257b91e52a4ac5815fb7413fd73788f893555a47f"

