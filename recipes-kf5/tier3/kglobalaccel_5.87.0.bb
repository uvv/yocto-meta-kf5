# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kglobalaccel-5.87.0.tar.xz"
SRC_URI[sha256sum] = "502e7f16eed132837e87c469cbdda474cd0d396075e6c519557b526f70af66f2"

