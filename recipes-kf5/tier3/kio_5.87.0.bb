# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kio-5.87.0.tar.xz"
SRC_URI[sha256sum] = "b91ed59de09c3b69c1eb50e6df2e087d912bda06a5d394763aa6e2e936cebc6b"

