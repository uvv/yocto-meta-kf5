# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/knotifyconfig-5.87.0.tar.xz"
SRC_URI[sha256sum] = "f03d30525b7fcdcf81071f647cef66c9306ede76ead6e546edb108f3e4f7a549"

