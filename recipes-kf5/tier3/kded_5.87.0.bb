# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kded-5.87.0.tar.xz"
SRC_URI[sha256sum] = "256bcaee8101bd3c9ec239bc3853fd8f7d5a98383858e509809a572a5a0bbca1"

