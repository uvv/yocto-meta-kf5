# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kwallet-5.87.0.tar.xz"
SRC_URI[sha256sum] = "59f13a6c9e0933e3e54c62a77717aa182be573f890a09e34369043e3208f58a8"

