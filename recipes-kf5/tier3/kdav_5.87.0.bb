# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kdav-5.87.0.tar.xz"
SRC_URI[sha256sum] = "bfccf61a54c7ddf10448ce735ac414ad26c5c39c1f762b0a7f85e2db38786169"

