# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/knewstuff-5.87.0.tar.xz"
SRC_URI[sha256sum] = "98f483edc5ac72cf70b40e9b9c571482d258f114ce0a75e47866d09147a6d176"

