# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/purpose-5.87.0.tar.xz"
SRC_URI[sha256sum] = "4dc3c6984216fbfd00f4754832e75cc51a90f426ce7d39611137f451a1a8b735"

