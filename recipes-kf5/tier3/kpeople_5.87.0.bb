# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kpeople-5.87.0.tar.xz"
SRC_URI[sha256sum] = "2aa8a2304fcc30595736c4e810467777e5bbce3e301af4439669ffecbbb35800"

