# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/ktexteditor-5.87.0.tar.xz"
SRC_URI[sha256sum] = "2f1f552f57c33dc213320d61c8b5e8470aa75e08aaf80c563773dcfd85e3b68d"

