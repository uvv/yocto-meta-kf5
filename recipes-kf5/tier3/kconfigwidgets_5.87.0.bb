# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kconfigwidgets-5.87.0.tar.xz"
SRC_URI[sha256sum] = "04b5e518e25ba967d15d705a6170d16ccc34d918c9135f1117551d7429b6a3a2"

