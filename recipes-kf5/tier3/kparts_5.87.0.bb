# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kparts-5.87.0.tar.xz"
SRC_URI[sha256sum] = "f12dd7de3aa7a7130240e2e29354bb3738fe559989a61638b876c3a9f3f6b24a"

