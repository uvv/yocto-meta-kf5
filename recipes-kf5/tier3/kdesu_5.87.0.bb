# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kdesu-5.87.0.tar.xz"
SRC_URI[sha256sum] = "795a4f2a8b7b530dfcd40021d506d0e4e6acd05acf1a7c08704102b0181c6b15"

