# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/plasma-framework-5.87.0.tar.xz"
SRC_URI[sha256sum] = "4b2ad68437b22647d4c29813176b9e1a199e78b1d49f64959b36a325cf22fcf8"

