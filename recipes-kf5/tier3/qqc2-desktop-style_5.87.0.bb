# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/qqc2-desktop-style-5.87.0.tar.xz"
SRC_URI[sha256sum] = "07ab65d080f7f3a6d9076d90b72e9dfb3f3a6caa4e01472a1d261ef7cacb240a"

