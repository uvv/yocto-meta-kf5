# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kbookmarks-5.87.0.tar.xz"
SRC_URI[sha256sum] = "dd268cb6d437cf74e46468561db465b32dac4e9c93a17113e83be1a62fd09c4e"

