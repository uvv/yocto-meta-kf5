# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/ktextwidgets-5.87.0.tar.xz"
SRC_URI[sha256sum] = "d9a5d3049ec1aab8a74fb1f83f998566195544024f33a92f45d938c911c7f511"

