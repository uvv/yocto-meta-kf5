# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kiconthemes-5.87.0.tar.xz"
SRC_URI[sha256sum] = "471ac6e36661d95d6ae966bb0357478dad2ae876b1f5e092e71d9a36fc589011"

