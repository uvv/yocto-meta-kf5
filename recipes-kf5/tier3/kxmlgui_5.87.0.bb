# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kxmlgui-5.87.0.tar.xz"
SRC_URI[sha256sum] = "b1185b12fe79efac59fe17398d6640c61e571575ca0d25ea80f15d2d33d0daf2"

