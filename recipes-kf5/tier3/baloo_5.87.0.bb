# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/baloo-5.87.0.tar.xz"
SRC_URI[sha256sum] = "8b6b55a300acb33d51ebda930c53fea2c393032d897d4c084d016b3a8d85a540"

