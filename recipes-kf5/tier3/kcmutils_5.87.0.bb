# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kcmutils-5.87.0.tar.xz"
SRC_URI[sha256sum] = "30cccaf9a5e6f7505b3f5628d274bb8d07c46d42da758616e670cdfa848bec26"

