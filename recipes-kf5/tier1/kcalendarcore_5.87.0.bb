# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kcalendarcore-5.87.0.tar.xz"
SRC_URI[sha256sum] = "166054f2a4975263e83f00110d4345c53ce82bc63b9789618231b9dc1be0e245"

