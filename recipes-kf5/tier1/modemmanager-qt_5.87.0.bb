# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/modemmanager-qt-5.87.0.tar.xz"
SRC_URI[sha256sum] = "33834799edfb33f015a884830b56014830fc34229fee36e27c2335084546e9a9"

