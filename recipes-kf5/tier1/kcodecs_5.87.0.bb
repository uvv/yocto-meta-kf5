# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kcodecs-5.87.0.tar.xz"
SRC_URI[sha256sum] = "0752156e85320731776f727ff171b2308e0f0c511efc4f2158dd763ed75368b9"

