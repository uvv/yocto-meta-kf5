# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kwidgetsaddons-5.87.0.tar.xz"
SRC_URI[sha256sum] = "2b119b3f886131009d24e139a313fb27c2ed3f681534d9297ade40153b4dfb01"

