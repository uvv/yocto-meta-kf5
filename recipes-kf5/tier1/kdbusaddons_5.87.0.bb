# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kdbusaddons-5.87.0.tar.xz"
SRC_URI[sha256sum] = "d75ca847fef7f324f97e7b3cdb45f7fa3cc2c36bd44ca35062589534d5ceb593"

