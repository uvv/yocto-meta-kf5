# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/bluez-qt-5.87.0.tar.xz"
SRC_URI[sha256sum] = "9fa942a16768bea53bbdaa6563ca0a7f04cee1820318e04c04abbdd6621e13ea"

