# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kitemmodels-5.87.0.tar.xz"
SRC_URI[sha256sum] = "22f139281ba32a15d3721168dc08e496d80f9894182967ac455873bbdc45a7e3"

