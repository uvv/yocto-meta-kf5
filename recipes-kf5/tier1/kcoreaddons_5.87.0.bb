# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kcoreaddons-5.87.0.tar.xz"
SRC_URI[sha256sum] = "d29fe93b58fec0edb3387d5bf5290366e64a5975df44c1b062cc4e29cfbfeda6"

