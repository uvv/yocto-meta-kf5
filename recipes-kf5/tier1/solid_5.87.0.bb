# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/solid-5.87.0.tar.xz"
SRC_URI[sha256sum] = "2a63b8a5866ee206337c1c7c6dfd62fe110dd9e6cf0197521a4992eb3361ac7e"

