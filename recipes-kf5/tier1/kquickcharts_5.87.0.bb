# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kquickcharts-5.87.0.tar.xz"
SRC_URI[sha256sum] = "6833af044f9150fba61ac09fa5cd2d67860f9c7c6ed0625d8dee2618398da4b7"

