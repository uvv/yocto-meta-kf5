# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/threadweaver-5.87.0.tar.xz"
SRC_URI[sha256sum] = "904db85af3f4cf5a7b0125264926d83405489feec66cacf675c67019c5fe17bf"

