# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kconfig-5.87.0.tar.xz"
SRC_URI[sha256sum] = "345bf5a68de968c3301aec16e1ec6eebd1c575a891464784aa0e177771e917d1"

