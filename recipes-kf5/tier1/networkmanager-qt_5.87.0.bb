# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/networkmanager-qt-5.87.0.tar.xz"
SRC_URI[sha256sum] = "bed4f9300adda630311721b50a5f306b55ac4ac720132675f0d955842aeb9a28"

