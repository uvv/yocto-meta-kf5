# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/oxygen-icons5-5.87.0.tar.xz"
SRC_URI[sha256sum] = "d50c6615f03e22fd1551137430fddcd38b6942aaf77570c765814e6baad8fd56"

