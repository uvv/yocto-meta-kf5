# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kimageformats-5.87.0.tar.xz"
SRC_URI[sha256sum] = "af6efb6232b8de670691f1983c26e385a64c9f5339d67ae004e97f6864c19830"

