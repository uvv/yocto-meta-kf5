# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/attica-5.87.0.tar.xz"
SRC_URI[sha256sum] = "b8a1a8427b721221a975eb5f078864246cdcbbfd97ca4e48d17280c72bcd4e72"

