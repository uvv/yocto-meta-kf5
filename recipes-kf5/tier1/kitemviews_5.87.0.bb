# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kitemviews-5.87.0.tar.xz"
SRC_URI[sha256sum] = "6f51463ae83f40df23d86037c5cda11caa862651178d796ca02e0de6ad482fdb"

