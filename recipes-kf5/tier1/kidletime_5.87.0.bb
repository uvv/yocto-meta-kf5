# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kidletime-5.87.0.tar.xz"
SRC_URI[sha256sum] = "1707318cc5b3b88558607fdb9e81d8dc0f817fa50ebd41e9ac8db014053017d4"

