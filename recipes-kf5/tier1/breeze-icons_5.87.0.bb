# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/breeze-icons-5.87.0.tar.xz"
SRC_URI[sha256sum] = "10154828dfa90bc157e32092b91022aea3c01e92aba7c320a261816532b1c7a5"

