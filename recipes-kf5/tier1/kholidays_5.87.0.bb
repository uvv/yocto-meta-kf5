# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kholidays-5.87.0.tar.xz"
SRC_URI[sha256sum] = "f5bfb0cd8668ceab5494bc72ee59a4e2a97ab735813ba1b70b34735ee70e45b1"

