# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kwayland-5.87.0.tar.xz"
SRC_URI[sha256sum] = "ac3aab3018e0910213c44a2ad219fd551d3ad2cbb1010adc20720db6f3a5990e"

