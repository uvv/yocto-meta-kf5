# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kguiaddons-5.87.0.tar.xz"
SRC_URI[sha256sum] = "90067fd81f6f25b0c5a1284c993fd04ea7212e33cf678744076935e484e808cd"

