# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kdnssd-5.87.0.tar.xz"
SRC_URI[sha256sum] = "dc3693968a0e699617fa2191add6114e39a3a01e9b6dcb90ff6a878acb8948e7"

