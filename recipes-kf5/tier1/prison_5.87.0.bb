# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/prison-5.87.0.tar.xz"
SRC_URI[sha256sum] = "07694637a1463882b4108fa6c8283324b35ffe471c27cfaa25fb0cf589b7686d"

