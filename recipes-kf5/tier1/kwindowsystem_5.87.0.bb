# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kwindowsystem-5.87.0.tar.xz"
SRC_URI[sha256sum] = "424962f23bd03188ef2bb5854e77cac90b6b70632fcc1ccbed86bc72b9bab567"

