# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/sonnet-5.87.0.tar.xz"
SRC_URI[sha256sum] = "1ad6fe55c06eeed600511be34a374aa46e1cb1e5fea8007255468fa1679289b1"

