# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kcompletion-5.87.0.tar.xz"
SRC_URI[sha256sum] = "7780670b0a1e62975a01d8b30f6bdefd820b6cd7543e29852f8e19fedad38995"

