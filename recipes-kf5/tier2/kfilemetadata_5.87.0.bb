# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kfilemetadata-5.87.0.tar.xz"
SRC_URI[sha256sum] = "51703dab11c7847180ce1d0ca62d3bc7ed3dba3992a4ef3ec2081596ef7f04da"

