# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kcrash-5.87.0.tar.xz"
SRC_URI[sha256sum] = "5908d771dcd1387941424369ca3ae974de91910a37987d8ab69458bc8b6d88d0"

