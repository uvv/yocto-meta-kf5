# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kjobwidgets-5.87.0.tar.xz"
SRC_URI[sha256sum] = "e56b845f6b609255420f6558d58b844dff68a35d67b67d78b98919f67c230f68"

