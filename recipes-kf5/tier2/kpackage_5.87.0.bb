# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kpackage-5.87.0.tar.xz"
SRC_URI[sha256sum] = "5eb749573a5ab5e2489d7409a11a616629f97f6e5e98cfea0164d6ee5790b0b6"

