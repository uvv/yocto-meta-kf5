# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kcontacts-5.87.0.tar.xz"
SRC_URI[sha256sum] = "9bd17db7b520df1f3a45f26e40312cdf18158e66f610d656cfe214dd768efe0b"

