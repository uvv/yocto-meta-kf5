# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kunitconversion-5.87.0.tar.xz"
SRC_URI[sha256sum] = "90fc2f3b69fd6705454e83bfc5451b1059f33bddca1749e29113641b81debeac"

