# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kactivities-5.87.0.tar.xz"
SRC_URI[sha256sum] = "e956e96a103f82ae62c62acc4ada41e116098781c7314bac65bc81d3d37bd6fa"

