# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.87/kauth-5.87.0.tar.xz"
SRC_URI[sha256sum] = "f1d1c00af4d6ac139b34073bb5c308de9f5f81d5bb75a3a3ffc05ab10f2278bd"

